From 2c6797bc97d7c92284dc3c0ed27f97ace4e5cfb9 Mon Sep 17 00:00:00 2001
From: Kazuki Yamaguchi <k@rhe.jp>
Date: Mon, 31 May 2021 11:44:05 +0900
Subject: [PATCH] test/openssl/utils: remove dup_public helper method

It uses deprecated PKey::{RSA,DSA,DH}#set_* methods, which will not
work with OpenSSL 3.0. The same can easily be achieved using
PKey#public_to_der regardless of the key kind.
---
 test/openssl/test_pkey_dh.rb  |  8 +++++---
 test/openssl/test_pkey_dsa.rb | 15 +++++++++++----
 test/openssl/test_pkey_ec.rb  | 15 +++++++++++----
 test/openssl/test_pkey_rsa.rb | 31 +++++++++++++++++--------------
 test/openssl/utils.rb         | 26 --------------------------
 5 files changed, 44 insertions(+), 51 deletions(-)

diff --git a/test/openssl/test_pkey_dh.rb b/test/openssl/test_pkey_dh.rb
index f80af8f841..757704caf6 100644
--- a/test/openssl/test_pkey_dh.rb
+++ b/test/openssl/test_pkey_dh.rb
@@ -40,12 +40,14 @@ def test_derive_key
 
   def test_DHparams
     dh1024 = Fixtures.pkey("dh1024")
+    dh1024params = dh1024.public_key
+
     asn1 = OpenSSL::ASN1::Sequence([
       OpenSSL::ASN1::Integer(dh1024.p),
       OpenSSL::ASN1::Integer(dh1024.g)
     ])
     key = OpenSSL::PKey::DH.new(asn1.to_der)
-    assert_same_dh dup_public(dh1024), key
+    assert_same_dh dh1024params, key
 
     pem = <<~EOF
     -----BEGIN DH PARAMETERS-----
@@ -55,9 +57,9 @@ def test_DHparams
     -----END DH PARAMETERS-----
     EOF
     key = OpenSSL::PKey::DH.new(pem)
-    assert_same_dh dup_public(dh1024), key
+    assert_same_dh dh1024params, key
     key = OpenSSL::PKey.read(pem)
-    assert_same_dh dup_public(dh1024), key
+    assert_same_dh dh1024params, key
 
     assert_equal asn1.to_der, dh1024.to_der
     assert_equal pem, dh1024.export
diff --git a/test/openssl/test_pkey_dsa.rb b/test/openssl/test_pkey_dsa.rb
index 147e50176b..0994607f21 100644
--- a/test/openssl/test_pkey_dsa.rb
+++ b/test/openssl/test_pkey_dsa.rb
@@ -138,6 +138,8 @@ def test_DSAPrivateKey_encrypted
 
   def test_PUBKEY
     dsa512 = Fixtures.pkey("dsa512")
+    dsa512pub = OpenSSL::PKey::DSA.new(dsa512.public_to_der)
+
     asn1 = OpenSSL::ASN1::Sequence([
       OpenSSL::ASN1::Sequence([
         OpenSSL::ASN1::ObjectId("DSA"),
@@ -153,7 +155,7 @@ def test_PUBKEY
     ])
     key = OpenSSL::PKey::DSA.new(asn1.to_der)
     assert_not_predicate key, :private?
-    assert_same_dsa dup_public(dsa512), key
+    assert_same_dsa dsa512pub, key
 
     pem = <<~EOF
     -----BEGIN PUBLIC KEY-----
@@ -166,10 +168,15 @@ def test_PUBKEY
     -----END PUBLIC KEY-----
     EOF
     key = OpenSSL::PKey::DSA.new(pem)
-    assert_same_dsa dup_public(dsa512), key
+    assert_same_dsa dsa512pub, key
+
+    assert_equal asn1.to_der, key.to_der
+    assert_equal pem, key.export
 
-    assert_equal asn1.to_der, dup_public(dsa512).to_der
-    assert_equal pem, dup_public(dsa512).export
+    assert_equal asn1.to_der, dsa512.public_to_der
+    assert_equal asn1.to_der, key.public_to_der
+    assert_equal pem, dsa512.public_to_pem
+    assert_equal pem, key.public_to_pem
   end
 
   def test_read_DSAPublicKey_pem
diff --git a/test/openssl/test_pkey_ec.rb b/test/openssl/test_pkey_ec.rb
index 4b6df0290f..d62f1b5eb8 100644
--- a/test/openssl/test_pkey_ec.rb
+++ b/test/openssl/test_pkey_ec.rb
@@ -210,6 +210,8 @@ def test_ECPrivateKey_encrypted
 
   def test_PUBKEY
     p256 = Fixtures.pkey("p256")
+    p256pub = OpenSSL::PKey::EC.new(p256.public_to_der)
+
     asn1 = OpenSSL::ASN1::Sequence([
       OpenSSL::ASN1::Sequence([
         OpenSSL::ASN1::ObjectId("id-ecPublicKey"),
@@ -221,7 +223,7 @@ def test_PUBKEY
     ])
     key = OpenSSL::PKey::EC.new(asn1.to_der)
     assert_not_predicate key, :private?
-    assert_same_ec dup_public(p256), key
+    assert_same_ec p256pub, key
 
     pem = <<~EOF
     -----BEGIN PUBLIC KEY-----
@@ -230,10 +232,15 @@ def test_PUBKEY
     -----END PUBLIC KEY-----
     EOF
     key = OpenSSL::PKey::EC.new(pem)
-    assert_same_ec dup_public(p256), key
+    assert_same_ec p256pub, key
+
+    assert_equal asn1.to_der, key.to_der
+    assert_equal pem, key.export
 
-    assert_equal asn1.to_der, dup_public(p256).to_der
-    assert_equal pem, dup_public(p256).export
+    assert_equal asn1.to_der, p256.public_to_der
+    assert_equal asn1.to_der, key.public_to_der
+    assert_equal pem, p256.public_to_pem
+    assert_equal pem, key.public_to_pem
   end
 
   def test_ec_group
diff --git a/test/openssl/test_pkey_rsa.rb b/test/openssl/test_pkey_rsa.rb
index 5e127f5407..4548bdb2cf 100644
--- a/test/openssl/test_pkey_rsa.rb
+++ b/test/openssl/test_pkey_rsa.rb
@@ -201,7 +201,7 @@ def test_sign_verify_pss
 
   def test_encrypt_decrypt
     rsapriv = Fixtures.pkey("rsa-1")
-    rsapub = dup_public(rsapriv)
+    rsapub = OpenSSL::PKey.read(rsapriv.public_to_der)
 
     # Defaults to PKCS #1 v1.5
     raw = "data"
@@ -216,7 +216,7 @@ def test_encrypt_decrypt
 
   def test_encrypt_decrypt_legacy
     rsapriv = Fixtures.pkey("rsa-1")
-    rsapub = dup_public(rsapriv)
+    rsapub = OpenSSL::PKey.read(rsapriv.public_to_der)
 
     # Defaults to PKCS #1 v1.5
     raw = "data"
@@ -346,13 +346,15 @@ def test_RSAPrivateKey_encrypted
 
   def test_RSAPublicKey
     rsa1024 = Fixtures.pkey("rsa1024")
+    rsa1024pub = OpenSSL::PKey::RSA.new(rsa1024.public_to_der)
+
     asn1 = OpenSSL::ASN1::Sequence([
       OpenSSL::ASN1::Integer(rsa1024.n),
       OpenSSL::ASN1::Integer(rsa1024.e)
     ])
     key = OpenSSL::PKey::RSA.new(asn1.to_der)
     assert_not_predicate key, :private?
-    assert_same_rsa dup_public(rsa1024), key
+    assert_same_rsa rsa1024pub, key
 
     pem = <<~EOF
     -----BEGIN RSA PUBLIC KEY-----
@@ -362,11 +364,13 @@ def test_RSAPublicKey
     -----END RSA PUBLIC KEY-----
     EOF
     key = OpenSSL::PKey::RSA.new(pem)
-    assert_same_rsa dup_public(rsa1024), key
+    assert_same_rsa rsa1024pub, key
   end
 
   def test_PUBKEY
     rsa1024 = Fixtures.pkey("rsa1024")
+    rsa1024pub = OpenSSL::PKey::RSA.new(rsa1024.public_to_der)
+
     asn1 = OpenSSL::ASN1::Sequence([
       OpenSSL::ASN1::Sequence([
         OpenSSL::ASN1::ObjectId("rsaEncryption"),
@@ -381,7 +385,7 @@ def test_PUBKEY
     ])
     key = OpenSSL::PKey::RSA.new(asn1.to_der)
     assert_not_predicate key, :private?
-    assert_same_rsa dup_public(rsa1024), key
+    assert_same_rsa rsa1024pub, key
 
     pem = <<~EOF
     -----BEGIN PUBLIC KEY-----
@@ -392,10 +396,15 @@ def test_PUBKEY
     -----END PUBLIC KEY-----
     EOF
     key = OpenSSL::PKey::RSA.new(pem)
-    assert_same_rsa dup_public(rsa1024), key
+    assert_same_rsa rsa1024pub, key
+
+    assert_equal asn1.to_der, key.to_der
+    assert_equal pem, key.export
 
-    assert_equal asn1.to_der, dup_public(rsa1024).to_der
-    assert_equal pem, dup_public(rsa1024).export
+    assert_equal asn1.to_der, rsa1024.public_to_der
+    assert_equal asn1.to_der, key.public_to_der
+    assert_equal pem, rsa1024.public_to_pem
+    assert_equal pem, key.public_to_pem
   end
 
   def test_pem_passwd
@@ -482,12 +491,6 @@ def test_private_encoding_encrypted
     assert_same_rsa rsa1024, OpenSSL::PKey.read(pem, "abcdef")
   end
 
-  def test_public_encoding
-    rsa1024 = Fixtures.pkey("rsa1024")
-    assert_equal dup_public(rsa1024).to_der, rsa1024.public_to_der
-    assert_equal dup_public(rsa1024).to_pem, rsa1024.public_to_pem
-  end
-
   def test_dup
     key = Fixtures.pkey("rsa1024")
     key2 = key.dup
diff --git a/test/openssl/utils.rb b/test/openssl/utils.rb
index c1d737b2ab..f664bd3074 100644
--- a/test/openssl/utils.rb
+++ b/test/openssl/utils.rb
@@ -313,32 +313,6 @@ def check_component(base, test, keys)
       assert_equal base.send(comp), test.send(comp)
     }
   end
-
-  def dup_public(key)
-    case key
-    when OpenSSL::PKey::RSA
-      rsa = OpenSSL::PKey::RSA.new
-      rsa.set_key(key.n, key.e, nil)
-      rsa
-    when OpenSSL::PKey::DSA
-      dsa = OpenSSL::PKey::DSA.new
-      dsa.set_pqg(key.p, key.q, key.g)
-      dsa.set_key(key.pub_key, nil)
-      dsa
-    when OpenSSL::PKey::DH
-      dh = OpenSSL::PKey::DH.new
-      dh.set_pqg(key.p, nil, key.g)
-      dh
-    else
-      if defined?(OpenSSL::PKey::EC) && OpenSSL::PKey::EC === key
-        ec = OpenSSL::PKey::EC.new(key.group)
-        ec.public_key = key.public_key
-        ec
-      else
-        raise "unknown key type"
-      end
-    end
-  end
 end
 
 module OpenSSL::Certs
-- 
2.32.0

